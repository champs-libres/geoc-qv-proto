# call base settings
from main.settings import *

# a secret key
SECRET_KEY = 'dsqdm7&ot6xsr&-isncic2kve$l@r)^=0n)f5+*rgn2)kki5)^c0mkqsdqsd'

DEBUG=True

# actual allowed hosts
ALLOWED_HOSTS = ['10.0.0.1', 'localhost','0.0.0.0']

# in case you want more applications loaded
# INSTALLED_APPS.append('django_python3_ldap')

# an actual database setting
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'HOST': 'db',
        'PORT': 5432,
        'NAME': 'postgres',
        'PASSWORD': 'postgres',
        'USER': 'postgres',
    },
}

# specific config for layers database
LAYERS_DB = {
    'ENGINE': 'django.contrib.gis.db.backends.postgis',
    'HOST': 'x.x.x.x',
    'NAME': 'dbname',
    'PASSWORD': 'password',
    'USER': 'username',
}

# here's a little loop to point to your postgis schemas
LAYERS_SCHEMAS = [
    'shema_name_0',
    'shema_name_1',
    'shema_name_2',
    'shema_name_3',
    'shema_name_4',
]

for schema in LAYERS_SCHEMAS:
    db_config = LAYERS_DB.copy()
    db_config.update({
        'OPTIONS': {
            'options': '-c search_path={},public'.format(schema),
        },
    })
    DATABASES[schema] = db_config

# clients are declared here
CLIENTS = [
    {
        'route' : 'compose',
        'name' : {
            'fr': 'Studio',
            'nl': 'Studio',
        }
    },
    {
        'route' : 'dashboard',
        'name' : {
            'fr': 'Dashboard',
            'nl': 'Dashboard',
        }
    },
    {
        'route' : 'view',
        'name' : {
            'fr': 'Atlas',
            'nl': 'Atlas',
        }
    },
    {
        'route' : 'embed'
    },
    {
        'route' : 'login'
    }
]
CLIENTS_DEFAULT = 'dashboard'

MAX_DECIMAL_DIGITS = 2

DEFAULT_GROUP = 'sdi:geodata'
PUBLIC_GROUP = 'sdi:public'


# Sorry, but we really need cache
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/home/pierre/System/var/cache/sdi',
        'OPTIONS': {
            'MAX_ENTRIES': 200000
        }
    },
    'layers': {
        'BACKEND': 'diskcache.DjangoCache',
        'LOCATION': '/home/pierre/System/var/cache/sdi/layers',
        'TIMEOUT': 60 * 60 * 24,
        'SHARDS': 4,
        'DATABASE_TIMEOUT': 1.0,
        'OPTIONS': {
            'size_limit': 2**32  # 4 gigabytes
        }
    }
}

# a django setting
MEDIA_ROOT = '/var/www/sdi/'

LANGUAGES = [
    ('fr', 'French'),
    ('nl', 'Dutch'),
]

LANGUAGE_CODE = 'fr'
