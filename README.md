

# Steps before using docker-compose

## 1. Ramener le code du dépot cartofixer dans le dossier `cartofixer` :

```
$ mkdir cartofixer
$ cd cartofixer
$ git clone git@gitlab.com:cartofixer/cartofixer.git
```


## 2. Mettre le style (si on utilise dans /cartofixer_style


```
$ mkdir cartofixer-style
$ cd cartofixer-style
$ git clone git@gitlab.com:cartofixer/cartofixer.git
```

## 3. Création des clients

```
        $ docker-compose run --rm clients bash
clients-run $ export WEBPACK_OPTIONS=--env.PLATFORM=/cartofixer-style/
clients-run $ ./build dashboard
```


## 4. envoi de l'img docker sdi sur gitlab


```
$ docker login registry.gitlab.com
$ docker-compose build sdi
$ docker push registry.gitlab.com/champs-libres/geoc-qv-proto
```